ARIN Number Resource Policy Manual Git Repository

Latest version: 2020.1 - 31 March 2020

This is a repository for ARIN’s Number Resource Policy Manual (NRPM). It is also available at: https://www.arin.net/policy/.

After accessing the repository, select nrpm.md from the file list to view the current NRPM version. To view historical versions:

Select "Commits" from the navigation menu.
In the list of versions that have been committed, under the "Commit" column, choose the commit hash (ee521be for example) of the version you want to view. This will open up a viewer where you can view the changes involved in that version, as tracked in-line changes.
To view the selected NRPM version without changes shown, select “View file” from the heading bar. To view a side-by-side comparison of the selected version and the preceding version, select “Side-by-side diff."

Number resource policies in the ARIN region are created in accordance with the “Policy Development Process” (https://www.arin.net/policy/pdp.html). The status of current and historical policy proposals can be found on the “Draft Policies and Proposals” page (https://www.arin.net/policy/proposals/).

The official NRPM Change Log can be found at: http://www.arin.net/policy/nrpm_changelog.html
